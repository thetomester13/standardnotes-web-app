FROM cloudron/base:2.0.0@sha256:f9fea80513aa7c92fe2e7bf3978b54c8ac5222f47a9a32a7f8833edf0eb5a4f4

ENV PROJECT_REPO=https://github.com/standardnotes/web
ENV RELEASE=3.3.1

ENV EXTENSIONS_MANAGER_REPO=https://github.com/sn-extensions/extensions-manager
ENV EXTENSIONS_MANAGER_RELEASE=1.2.4
ENV BATCH_MANAGER_REPO=https://github.com/sn-extensions/batch-manager
ENV BATCH_MANAGER_RELEASE=3897593b98fe46505a8bf8a8e22fd64319dc5900

RUN mkdir -p /app/code /app/pkg /app/data/web /app/data/web/tmp /app/data/web/tmp/cache /app/data/web/tmp/pids /app/data/web/tmp/sockets

# Standardnotes Web app
RUN mkdir -p /app/code/web
RUN curl -L "${PROJECT_REPO}/archive/${RELEASE}.tar.gz" \
    | tar -xz --strip-components=1 -C /app/code/web

WORKDIR /app/code/web

# Enable Extensions Manager and Batch Manager
RUN curl -L "${EXTENSIONS_MANAGER_REPO}/archive/${EXTENSIONS_MANAGER_RELEASE}.tar.gz" \
    | tar -xz --strip-components=1 -C /app/code/web/public/extensions/extensions-manager

RUN curl -L "${BATCH_MANAGER_REPO}/archive/${BATCH_MANAGER_RELEASE}.tar.gz" \
    | tar -xz --strip-components=1 -C /app/code/web/public/extensions/batch-manager

RUN gem install bundler

RUN npm run build

RUN touch /app/code/web/log/production.log && \
    cp -r /app/code/web/log /app/data/web/log && \
    mv /app/code/web/log /app/code/web/log.original && \
    ln -s /app/data/web/log /app/code/web/log

RUN ln -s /app/data/web/tmp /app/code/web/tmp

COPY start.sh /app/pkg/

CMD [ "/app/pkg/start.sh" ]
