#!/bin/bash

set -eu

echo "==> Changing permissions"
chmod 777 -R /app/data/web/log /app/data/web/tmp

NOTES_SYNCING_SERVER=${CLOUDRON_WEBADMIN_ORIGIN/my/notessyncserver}
export SF_DEFAULT_SERVER=${NOTES_SYNCING_SERVER}

export EXTENSIONS_MANAGER_LOCATION=extensions/extensions-manager/dist/index.html
export BATCH_MANAGER_LOCATION=extensions/batch-manager/dist/index.min.html

echo "==> Staring Standard Notes"

bundle exec rails server -b 0.0.0.0
